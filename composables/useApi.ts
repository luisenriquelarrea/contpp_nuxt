export const useApi = () => {
    const authentication = async (username: string, password: string) => {
        return await $fetch('/api/otherService/authenticate', {
            method: 'POST',
            body: JSON.stringify({
                username: username, 
                password: password
            })
        });
    }

    const countFilteredList = async (seccionMenu: string, filter: any) => {
        return await fetch('/api/otherService/'+seccionMenu+'/countFilteredList', {
            method: 'POST',
            body: JSON.stringify(filter),
        });
    }

    const deleteRecord = async (seccionMenu: string, id: number) => {
        return await $fetch('/api/otherService/'+seccionMenu+"/"+id, {
            method: 'DELETE'
        });
    }

    const getBreadcrumbs = async (seccionMenuId:number, grupoId: number) => {
        return await $fetch('/api/otherService/accion_grupo/allowed_breadcrumbs', {
            method: 'POST',
            body: JSON.stringify({
                seccionMenuId: seccionMenuId,
                grupoId: grupoId
            })
        });
    }

    const getById = async (seccionMenu: string, id: number) => {
        return await $fetch('/api/otherService/'+seccionMenu+"/"+id, {
            method: 'GET'
        });
    }

    const getInputs = async (seccionMenuId:number, columna: string) => {
        return await $fetch('/api/otherService/seccion_menu_input/inputs', {
            method: 'POST',
            body: JSON.stringify({ 
                'seccionMenuId': seccionMenuId,
                'columna': columna
            })
        });
    }

    const getNavbarActions = async (seccionMenuId:number, grupoId: number) => {
        return await $fetch('/api/otherService/accion_grupo/allowed_navbar', {
            method: 'POST',
            body: JSON.stringify({
                seccionMenuId: seccionMenuId,
                grupoId: grupoId
            })
        });
    }

    const getNavLinks = async (grupoId: number) => {
        return await $fetch('/api/otherService/accion_grupo/allowed_menus', {
            method: 'POST',
            body: JSON.stringify({
                grupoId: grupoId
            })
        });
    }

    const getSeccionMenu = async (seccionMenu: string) => {
        return await $fetch('/api/otherService/seccion_menu/descripcion', {
            method: 'POST',
            body: JSON.stringify({
                descripcion: seccionMenu
            })
        });
    }

    const getSeccionMenuListFiltered = async(seccionMenu: string, formdata: any) => {
        return await $fetch('/api/otherService/'+seccionMenu+"/filteredList", {
            method: 'POST',
            body: JSON.stringify(formdata),
        });
    }

    const getTableActions = async (seccionMenuId:number, grupoId: number) => {
        return await $fetch('/api/otherService/accion_grupo/allowed_table_actions', {
            method: 'POST',
            body: JSON.stringify({
                seccionMenuId: seccionMenuId,
                grupoId: grupoId
            })
        });
    }

    const save = async(seccionMenu: string, formdata: any) => {
        return await $fetch('/api/otherService/'+seccionMenu+"/add", {
            method: 'POST',
            body: JSON.stringify(formdata),
        });
    }

    const updateRecord = async (seccionMenu: string, id: number, formdata: any) => {
        return await $fetch('/api/otherService/'+seccionMenu+"/"+id, {
            method: 'PUT',
            body: JSON.stringify(formdata),
        });
    }

    return {
        authentication,
        countFilteredList,
        deleteRecord,
        getBreadcrumbs,
        getById,
        getNavLinks,
        getSeccionMenu,
        getInputs,
        getNavbarActions,
        getTableActions,
        getSeccionMenuListFiltered,
        updateRecord,
        save
    }
}