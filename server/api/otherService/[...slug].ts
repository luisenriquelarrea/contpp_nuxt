const config = useRuntimeConfig();

export default defineEventHandler((event) => {
    return proxyRequest(
        event,
        `${config.public.baseURL}${event.context.params!.slug}`, { 
            headers: { 
                [config.apiKey]: config.apiSecret, 
                "Content-Type": "application/json", 
            }
    });
});