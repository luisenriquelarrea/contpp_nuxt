import { defineStore } from 'pinia'
const { authentication } = useApi();

export const useAuthenticationStore = defineStore('auth', () => {
    const credentials = ref(null);

    const isAuthenticated = computed( () => {
        return credentials.value;
    })

    const setCredentials = (data: any) => {
        credentials.value = data;
    };
    
    const getCredenctials = (username: string, password: string) => {
        return authentication(username, password);
    };

    return{
        credentials,
        getCredenctials,
        setCredentials,
        isAuthenticated
    }
},{persist: true})