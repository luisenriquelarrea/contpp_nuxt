// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: ['@/assets/css/main.scss'],
  devtools: { enabled: true },

  modules: [
    '@pinia/nuxt',
    '@pinia-plugin-persistedstate/nuxt',
  ],

  build: {
    transpile: ['pinia-plugin-persistedstate'],
  },

  app: { 
    head: {
      title: 'Nuxt Development',
      meta: [{
          'name': 'viewport',
          'content': 'width=device-width, initial-scale=1'
        },
      ],
    }
  },

  runtimeConfig: {
    apiKey: process.env.API_KEY,
    apiSecret: process.env.API_TOKEN,
    public: {
      baseURL: process.env.BASE_URL || 'http://127.0.0.1:8000/api/',
    },
  },

  compatibilityDate: '2024-08-15'
})